package db

import "fmt"

type User struct {
	UID   int
	Name  string
	Level int
	//	Prop  Prop
	//	Item  Item
}

type Item struct {
	ID    int
	Level int
	Prop  Prop
}

type Prop struct {
	ID int
	AP int
	DP int
	HP int
	MP int
}

type Users []User

func NewUser(uid int) *User {
	return &User{
		UID:   uid,
		Name:  fmt.Sprintf("user%d", uid),
		Level: 1,
	}
}
