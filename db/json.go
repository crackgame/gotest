package db

import (
	"encoding/json"
	"fmt"
	"functime"
)

var jsonCache map[int][]byte = map[int][]byte{}

func Json_InitData() {
	for i := 0; i < PlayerNum; i++ {
		uid := 1000 + i + 1

		_, found := jsonCache[uid]
		if found {
			continue
		}

		user := NewUser(uid)

		b, err := json.Marshal(user)
		if err != nil {
			fmt.Println("encode fail: " + err.Error())
		}

		jsonCache[uid] = b
	}
}

func Json_RunStress() {
	fc := functime.NewFuncTime()
	defer fc.End("Json_RunStress")

	//	wg := sync.WaitGroup{}

	for n := 0; n < PlayerNum; n++ {
		//		wg.Add(1)
		func() {
			for i := 1; i <= FriendNum; i++ {
				uid := 1000 + i
				b := jsonCache[uid]

				var user User
				json.Unmarshal(b, &user)

				_ = user
				_ = b

				//				users = append(users, user)

				//				fmt.Println("json get user:", user)
			}
			//			wg.Done()
		}()
	}

	//	wg.Wait()

	//	spew.Dump(users)
}
