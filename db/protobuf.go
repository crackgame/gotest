package db

import (
	"fmt"
	"functime"

	"hope_server/message/shared"

	"github.com/golang/protobuf/proto"
)

var protoCache map[int][]byte = map[int][]byte{}

func Proto_InitData() {
	for i := 0; i < PlayerNum; i++ {
		uid := 1000 + i + 1

		_, found := protoCache[uid]
		if found {
			continue
		}

		//		user := NewUser(uid)

		user := &shared.Player{
			Uid:   proto.Int64(int64(uid)),
			Name:  proto.String(fmt.Sprintf("user%v", uid)),
			Level: proto.Int(1),
		}

		b, err := proto.Marshal(user)
		if err != nil {
			fmt.Println("encode fail: " + err.Error())
		}

		protoCache[uid] = b
	}
}

func Proto_RunStress() {
	fc := functime.NewFuncTime()
	defer fc.End("Proto_RunStress")

	//	wg := sync.WaitGroup{}

	for n := 0; n < PlayerNum; n++ {
		//		wg.Add(1)
		func() {
			for i := 1; i <= FriendNum; i++ {
				uid := 1000 + i
				b := protoCache[uid]

				//var user User
				var user shared.Player
				proto.Unmarshal(b, &user)

				_ = user
				_ = b

				//				users = append(users, user)

				//				fmt.Println("proto get user:", *user.Uid, *user.Name, *user.Level)
			}
			//			wg.Done()
		}()
	}

	//	wg.Wait()

	//	spew.Dump(users)
}
