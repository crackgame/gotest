package db

import "functime"

var cache map[int]User = map[int]User{}

func Memory_InitData() {
	for i := 0; i < PlayerNum; i++ {
		uid := 1000 + i + 1

		_, found := cache[uid]
		if found {
			continue
		}

		user := NewUser(uid)
		cache[uid] = *user
	}
}

func Memory_RunStress() {
	fc := functime.NewFuncTime()
	defer fc.End("Memory_RunStress")

	//	wg := sync.WaitGroup{}

	for n := 0; n < PlayerNum; n++ {
		//		wg.Add(1)
		func() {
			for i := 1; i <= FriendNum; i++ {
				uid := 1000 + i
				user := cache[uid]

				//				users = append(users, user)

				_ = user
				//				fmt.Println("memory get user:", user)
			}
			//			wg.Done()
		}()
	}

	//	wg.Wait()

	//	spew.Dump(users)
}
