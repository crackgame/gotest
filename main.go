package main

import (
	"gotest/db"

	"sync"

	"functime"

	"github.com/pkg/profile"
)

func main() {
	fc := functime.NewFuncTime()
	defer fc.End("main")
	defer profile.Start(profile.CPUProfile, profile.ProfilePath(".")).Stop()

	wg := sync.WaitGroup{}

	wg.Add(1)
	go func() {
		db.Memory_InitData()
		db.Memory_RunStress()
		wg.Done()
	}()

	wg.Add(1)
	go func() {
		db.Mysql_InitData()
		db.Mysql_RunStress()
		wg.Done()
	}()

	wg.Add(1)
	go func() {
		db.Mongo_InitData()
		db.Mongo_RunStress()
		wg.Done()
	}()

	wg.Add(1)
	go func() {
		db.Redis_InitData()
		db.Redis_RunStress()
		wg.Done()
	}()

	wg.Add(1)
	go func() {
		db.Gob_InitData()
		db.Gob_RunStress()
		wg.Done()
	}()

	wg.Add(1)
	go func() {
		db.Json_InitData()
		db.Json_RunStress()
		wg.Done()
	}()

	wg.Add(1)
	go func() {
		db.Proto_InitData()
		db.Proto_RunStress()
		wg.Done()
	}()

	wg.Wait()
}

///*
//#include <stdio.h>
//
//extern void GoExportedFunc();
//
//void bar() {
//        printf("I am bar!\n");
//        GoExportedFunc();
//}
//*/
//import "C"
//
//func main() {
//	C.bar()
//}
